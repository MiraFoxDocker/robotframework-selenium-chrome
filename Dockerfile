FROM python:2.7.16-alpine3.9

LABEL description="Clone by eficode/robotframework-selenium images"

ENV SCREEN_WIDTH 1920
ENV SCREEN_HEIGHT 1080
ENV SCREEN_DEPTH 16
ENV DEPS="\
    chromium \
    chromium-chromedriver \
    udev \
    xvfb \
    python2-tkinter \
"

COPY requirements.txt /tmp/requirements.txt
COPY entrypoint_pabot.sh /opt/bin/entrypoint_pabot.sh
COPY entrypoint_robot.sh /opt/bin/entrypoint_robot.sh

RUN apk update ;\
    apk add --no-cache ${DEPS} ;\
    pip install --no-cache-dir -r /tmp/requirements.txt ;\
    # Chrome requires docker to have cap_add: SYS_ADMIN if sandbox is on.
    # Disabling sandbox and gpu as default.
    sed -i "s/self._arguments\ =\ \[\]/self._arguments\ =\ \['--no-sandbox',\ '--disable-gpu'\]/" $(python -c "import site; print(site.getsitepackages()[0])")/selenium/webdriver/chrome/options.py ;\
    # List packages and python modules installed
    apk info -vv | sort ;\
    pip freeze ;\
    # Cleanup
    rm -rf /var/cache/apk/* /tmp/requirements.txt

ENTRYPOINT [ "/opt/bin/entrypoint_pabot.sh" ]
